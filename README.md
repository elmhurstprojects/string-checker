# String Checker

Checks a string for unwanted items in a post, comment etc. Checks for profanity, keywords, urls, email addresses and HTML tags.

### Installation

```
composer require elmhurstprojects/string-checker
```

Then, if auto-discovery not available, add the service provider to app.php

```
\ElmhurstProjects\StringChecker\Providers\StringCheckerServiceProvider::class,
```

You can then publish the config files where you can change the words you wish to ban and flag.

```
php artisan vendor:publish
```
