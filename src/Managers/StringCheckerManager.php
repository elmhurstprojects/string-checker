<?php

namespace ElmhurstProjects\StringChecker\Managers;

use mofodojodino\ProfanityFilter\Check;

class StringCheckerManager
{
    protected $banned_word_checker;

    protected $flagged_word_checker;

    public function __construct()
    {
        $this->banned_word_checker = new Check(config('profanity.banned'));

        $this->flagged_word_checker = new Check(config('profanity.flagged'));
    }

    public function checkAll(string $string)
    {
        return (object)[
          'banned' => $this->checkForBannedWords($string),
          'flagged' => $this->checkForFlaggedWords($string),
          'website' => $this->checkForWebsite($string),
          'email' => $this->checkForEmailAddresses($string),
          'html' => $this->checkForHTML($string)
        ];
    }

    /**
     * Checks for extreme words
     * @param string $string
     * @return bool
     */
    public function checkForBannedWords(string $string):bool
    {
        return $this->banned_word_checker->hasProfanity($string);
    }

    /**
     * Checks for non-extreme words
     * @param string $string
     * @return bool
     */
    public function checkForFlaggedWords(string $string):bool
    {
        return $this->flagged_word_checker->hasProfanity($string);
    }

    /**
     * Check string doesn't contain email address
     * @param string $string
     * @return bool
     */
    public function checkForEmailAddresses(string $string):bool
    {
        return preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/si', $string) != 0;
    }

    /**
     * Check string for website address
     * @param string $string
     * @return bool
     */
    public function checkForWebsite(string $string):bool
    {
        return preg_match("(https|ftp|http|www)", $string) != 0;
    }

    /**
     * Check string for email address
     * @param string $string
     * @return bool
     */
    public function checkForHTML(string $string):bool
    {
        return $string != strip_tags($string);
    }

    /**
     * Set the array of banned words
     * @param array $banned_words
     * @return $this
     */
    public function setBannedWords(array $banned_words)
    {
        $this->banned_word_checker = new Check($banned_words);

        return $this;
    }

    /**
     * Set the flagged words array
     * @param array $flagged_words
     * @return $this
     */
    public function setFlaggedWords(array $flagged_words)
    {
        $this->flagged_word_checker = new Check($flagged_words);

        return $this;
    }
}
