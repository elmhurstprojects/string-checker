<?php

namespace ElmhurstProjects\StringChecker\Providers;

use Illuminate\Support\ServiceProvider;

class StringCheckerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/profanity.php' => config_path('profanity.php'),
        ], 'profanity');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    public function register()
    {

    }
}